#ifndef CLIENT_SERVER_MODBUS_COMMANDS_H
#define CLIENT_SERVER_MODBUS_COMMANDS_H

#include "modbus_msg.h"

#include <vector>
#include <string>

namespace modbus_conv {

/**
 * set up fields of dst (frame) from the source vector src
 * @param dst -- destination msg
 * @param src -- source vector
 * @param crc_required -- if true fill the crc field
 */
    void set_frame(modbus_msg_t &dst, const std::vector<uint8_t> &src, bool crc_required = false);

/**
 * set up fields of dst from scenario frame
 * @param dst -- destination msg
 * @param addr -- string command
 * @param args -- vector<uint8_t> args
 */
    void set_frame(modbus_msg_t &dst, uint8_t addr, uint8_t code, const std::vector<uint8_t> &args);

}

#endif //CLIENT_SERVER_MODBUS_COMMANDS_H
