#ifndef CLIENT_SERVER_MODBUS_MSG_H
#define CLIENT_SERVER_MODBUS_MSG_H

#include <utility>
#include <vector>
#include <cstdint>
#include <mutex>
#include <memory>

namespace modbus_conv {
/**
 * header -- vector of 2 bytes (address + function code)
 * data -- vector of 0 - 252 bytes
 * crc -- vector of 2 bytes
 */
    struct modbus_msg_t {
    public:
        modbus_msg_t(const std::vector<uint8_t> &h, const std::vector<uint8_t> &d, const std::vector<uint8_t> &c);

        explicit modbus_msg_t(const std::vector<uint8_t> &src, bool crc_required = false);

        modbus_msg_t() = default;

        ~modbus_msg_t() = default;

        std::vector<uint8_t> header{};
        std::vector<uint8_t> data{};
        std::vector<uint8_t> crc{};
    };

    typedef std::shared_ptr<modbus_msg_t> modbus_msg_t_ptr;

/**
 * set header to msg
 * @param msg -- current message
 * @param src -- current source vector
 */
    void set_header(modbus_msg_t &msg, const std::vector<uint8_t> &src);

/**
 * set data to msg
 * @param msg -- current message
 * @param src -- current source vector
 */
    void set_data(modbus_msg_t &msg, const std::vector<uint8_t> &src);

/**
 * set crc to msg
 * @param msg -- current message
 * @param src -- current source vector
 */
    void set_crc(modbus_msg_t &msg, const std::vector<uint8_t> &src);

/**
 * get header from msg
 * @param msg -- current message
 * @return header vector
 */
    std::vector<uint8_t> get_header(const modbus_msg_t &msg);

/**
 * get data from msg
 * @param msg
 * @return data vector
 */
    std::vector<uint8_t> get_data(const modbus_msg_t &msg);

/**
 * get crc from msg
 * @param msg
 * @return crc vector
 */
    std::vector<uint8_t> get_crc(const modbus_msg_t &msg);

/**
 * check msg is empty
 * @param msg
 * @return true if header data and crc is empty
 */
    bool is_empty(const modbus_msg_t &msg);


/**
 * check msg is valid
 * msg is not empty
 * header must be 2 bytes
 * data must be 0-252 bytes
 * crc must be 0 or 2 bytes
 * //TODO another checks ?
 * @param msg
 * @return true if msg is valid
 */
    bool is_valid_format(const modbus_msg_t &msg, bool crc_required = false);

/**
 * print message into log
 * @param msg -- current message
 */
    void print_msg(const modbus_msg_t &msg);

}

#endif //CLIENT_SERVER_MODBUS_MSG_H
