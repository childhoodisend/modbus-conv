#include "modbus_msg.h"
#include "modbus_frame.h"
#include "globals.h"

#include <stdexcept>
#include <algorithm>
#include <iostream>

namespace modbus_conv {

    modbus_msg_t::modbus_msg_t(const std::vector<uint8_t> &h, const std::vector<uint8_t> &d,
                               const std::vector<uint8_t> &c) {

        if (h.size() != 2) {
            throw std::runtime_error("modbus_msg_t() exc : h.size() != 2");
        }

        if (d.size() > 252) {
            throw std::runtime_error("modbus_msg_t() exc : d.size() > 252");
        }

        if (!(c.empty() || c.size() == 2)) {
            throw std::runtime_error("modbus_msg_t() exc : c.size() is invalid!");
        }

        //TODO another checks
        if (std::find(codes.begin(), codes.end(), h[1]) != codes.end()) {
            header = h;
            data = d;
            crc = c;
        }

    }

    modbus_msg_t::modbus_msg_t(const std::vector<uint8_t> &src, bool crc_required) {
        set_frame(*this, src, crc_required);
    }

    void set_header(modbus_msg_t &msg, const std::vector<uint8_t> &src) {
        msg.header = src;
    }


    void set_data(modbus_msg_t &msg, const std::vector<uint8_t> &src) {
        msg.data = src;
    }


    void set_crc(modbus_msg_t &msg, const std::vector<uint8_t> &src) {
        msg.crc = src;
    }


    std::vector<uint8_t> get_header(const modbus_msg_t &msg) {
        if (!msg.header.empty()) {
            return msg.header;
        } else {
            throw std::runtime_error("get_header() : msg.header.empty()");
        }
    }


    std::vector<uint8_t> get_data(const modbus_msg_t &msg) {
        if (!msg.data.empty()) {
            return msg.data;
        } else {
            throw std::runtime_error("get_data() : msg.data.empty()");
        }
    }


    std::vector<uint8_t> get_crc(const modbus_msg_t &msg) {
        if (!msg.crc.empty()) {
            return msg.crc;
        } else {
            throw std::runtime_error("crc() : msg.crc.empty()");
        }
    }

    bool is_empty(const modbus_msg_t &msg) {
        return (msg.header.empty() && msg.data.empty() && msg.crc.empty());
    }

    bool is_valid_format(const modbus_msg_t &msg, bool crc_required) {
        bool crc_check = true;

        const auto hsize = msg.header.size();
        const auto dsize = msg.data.size();

        if (crc_required) {
            const auto csize = msg.crc.size();
            crc_check = (csize == 0 || csize == 2);
        }

        return (hsize == 2) && (dsize > 0 && dsize <= 252) && crc_check;
    }

    void print_msg(const modbus_msg_t &msg) {
        std::cout << "-----HEADER-----" << std::endl;
        for (const auto &h: msg.header) {
            printf("0x%02X ", h);
        }
        std::cout << "\n";

        std::cout << "------DATA------" << std::endl;
        for (const auto &d: msg.data) {
            printf("0x%02X ", d);
        }
        std::cout << "\n";

        std::cout << "------CRC-------" << std::endl;
        for (const auto &c: msg.crc) {
            printf("0x%02X ", c);
        }
        std::cout << "\n";
        std::cout << "\n";
    }
}