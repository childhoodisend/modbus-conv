#include "modbus_commands.h"

#include <iostream>
#include "globals.h"

namespace modbus_conv {

//*****SET COMMANDS*****

    void comm_set_address(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::SLAVE_ADDR_BAUD_RATE) {
            throw std::runtime_error("comm_set_address() invalid src size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_address() invalid addr " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x0A}); // [addr, 0x0A]

        if (args[0] != 0x00) {
            throw std::runtime_error("comm_set_address() invalid " + std::to_string((uint16_t) args[0]));
        }
        if (args[1] != 0x01) {
            throw std::runtime_error("comm_set_address() invalid " + std::to_string((uint16_t) args[1]));
        }
        if (args[2] < 0x01 || args[2] > 0xF7) {
            throw std::runtime_error("comm_set_address() invalid " + std::to_string((uint16_t) args[2]));
        }
        set_data(dst, args); // [args]
    }

    void comm_set_baud(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::SLAVE_ADDR_BAUD_RATE) {
            throw std::runtime_error("comm_set_baud() invalid src size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_baud() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x0A}); // [addr, 0x0A]

        if (args[0] != 0xFF) {
            throw std::runtime_error("comm_set_baud() invalid " + std::to_string((uint16_t) args[0]));
        }
        if (args[1] != 0x00) {
            throw std::runtime_error("comm_set_baud() invalid " + std::to_string((uint16_t) args[1]));
        }
        if ((args[2] & 0x0F) < 0x05 || (args[2] & 0x0F) > 0x09) {
            throw std::runtime_error("comm_set_baud() invalid 1-4 bit " + std::to_string((uint16_t) (0x0F & args[2])));
        }
        if ((args[2] & 0xC0) > 0x02) {
            throw std::runtime_error("comm_set_baud() invalid 7-8 bit " + std::to_string((uint16_t) (0xC0 & args[2])));
        }

        set_data(dst, args); // [args]
    }

    void comm_set_conf_motor(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::CONF_MOTOR) {
            throw std::runtime_error("comm_set_conf_motor() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_conf_motor() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x64}); // [addr, 0x64]

        if (args[0] != 0x00 && args[0] != 0xFF) {
            throw std::runtime_error("comm_set_conf_motor() invalid " + std::to_string((uint16_t) args[0]));
        }
        if (args[8] > 0x09) {
            throw std::runtime_error("comm_set_conf_motor() invalid " + std::to_string((uint16_t) args[8]));
        }
        if (args[9] != 0x00 && args[9] != 0x01) {
            throw std::runtime_error("comm_set_conf_motor() invalid " + std::to_string((uint16_t) args[9]));
        }
        if (args[10] != 0x00 && args[10] != 0x01) {
            throw std::runtime_error("comm_set_conf_motor() invalid " + std::to_string((uint16_t) args[10]));
        }

        set_data(dst, args); // [args]
    }

    void comm_set_conf_el(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::CONF_EL) {
            throw std::runtime_error("comm_set_conf_el() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_conf_el() invalid " + std::to_string(addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x65}); // [addr, 0x65]

        for (size_t i = 0; i < 5; ++i) {
            if (args[i] != 0x00 && args[i] != 0xFF) {
                throw std::runtime_error("comm_set_conf_el() invalid " + std::to_string((uint16_t) args[i]));
            }
        }

        for (size_t i = 22; i < 25; ++i) {
            if (args[i] != 0x00 && args[i] != 0xFF) {
                throw std::runtime_error("comm_set_conf_el() invalid " + std::to_string((uint16_t) args[i]));
            }
        }

        set_data(dst, args); // [args]
    }

    void comm_set_limit(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::LIMIT_DATA) {
            throw std::runtime_error("comm_set_limit() invalid args size " + std::to_string(args.size()));
        }
        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_limit() invalid addr " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x66}); // [addr, 0x66]

        if (args[6] > 0x64) {
            throw std::runtime_error("comm_set_limit() invalid 6 " + std::to_string((uint16_t) args[6]));
        }

        for (size_t i = 17; i < 19; ++i) {
            if (args[i] != 0x00 && args[i] != 0xFF) {
                throw std::runtime_error("comm_set_limit() invalid " + std::to_string(i) + " " + std::to_string((uint16_t) args[i]));
            }
        }

        set_data(dst, args); // [args]
    }

    void comm_set_speed_linear_move(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::SPEED_LINEAR_MOVE) {
            throw std::runtime_error(
                    "comm_set_speed_linear_move() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_speed_linear_move() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x67}); // [addr, 0x67]

        set_data(dst, args); // [args]
    }

    void comm_set_brake_condition(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::BRAKE_CONDITION) {
            throw std::runtime_error("comm_set_brake_condition() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_brake_condition() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x47}); // [addr, 0x47]

        if (args[0] != 0x00 && args[0] != 0x01) {
            throw std::runtime_error("comm_set_brake_condition() invalid " + std::to_string((uint16_t) args[0]));
        }
        set_data(dst, args); // [args]
    }

    void comm_req_null_point(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {
        if (args.size() != TX_SIZE::NULL_POINT) {
            throw std::runtime_error("comm_set_null_point() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_set_null_point() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x6B}); // [addr, 0x6B]

        switch (args[0]) {
            case 0x00:
            case 0x02:
            case 0x03: {
                for (size_t i = 1; i < args.size(); ++i) {
                    if (args[i] != 0x00) {
                        throw std::runtime_error(
                                "comm_set_null_point() invalid args[i]" + std::to_string((uint16_t) args[i]));
                    }
                }
                break;
            }
            case 0x01: {
                break;
            }
            default: {
                throw std::runtime_error("comm_set_null_point() invalid args[0]" + std::to_string((uint16_t) args[0]));
            }
        }

        set_data(dst, args); // [args]
    }


    void comm_req_settings_requlator(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {
        if (args.size() != TX_SIZE::REGULATOR_SETTINGS) {
            throw std::runtime_error("comm_req_settings_requlator() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_req_settings_requlator() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x6C}); // [addr, 0x6C]

        if (args[0] > 0x05) {
            throw std::runtime_error(
                    "comm_req_settings_requlator() invalid args[0]" + std::to_string((uint16_t) args[0]));
        }

        switch (args[0]) {
            case 0x00: {
                break;
            }
            case 0x01: {
                break;
            }
            case 0x02:
            case 0x03:
            case 0x04:
            case 0x05: {
                for (size_t i = 1; i < args.size(); ++i) {
                    if (args[i] != 0x00) {
                        throw std::runtime_error(
                                "comm_set_null_point() invalid args[i]" + std::to_string((uint16_t) args[i]));
                    }
                }
                break;
            }
            default: {
                throw std::runtime_error("comm_set_null_point() invalid args[0]" + std::to_string((uint16_t) args[0]));
            }
        }

        set_data(dst, args); // [args]
    }

//*****SET COMMANDS*****


//*****REQUEST COMMANDS*****

    void comm_req_calibration_data(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (args.size() != TX_SIZE::CALIBRATION) {
            throw std::runtime_error("comm_req_calibration_data() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_req_calibration_data() invalid " + std::to_string((uint16_t) addr));
        }
        if (args[0] == 1 || args[0] == 3 || args[0] > 12) {
            throw std::runtime_error("comm_req_calibration_data() invalid " + std::to_string((uint16_t) args[0]));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x41}); // [addr, 0x41]
        set_data(dst, args); // [args]
    }

    void comm_req_firmware_and_configuration(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (!args.empty()) {
            throw std::runtime_error(
                    "comm_req_firmware_and_configuration() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error(
                    "comm_req_firmware_and_configuration() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x42}); // [addr, 0x42]
    }

    void comm_req_diagnostic(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (!args.empty()) {
            throw std::runtime_error("comm_req_diagnostic() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_req_diagnostic() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x43}); // [addr, 0x43]
    }

    void comm_req_condition_sensor(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {

        if (!args.empty()) {
            throw std::runtime_error(
                    "comm_req_condition_sensor() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_req_condition_sensor() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x48}); // [addr, 0x48]
    }

    void comm_req_get_pos(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {
        if (!args.empty()) {
            throw std::runtime_error(
                    "comm_req_get_pos() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_req_get_pos() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x6A}); // [addr, 0x6A]
    }



//*****REQUEST COMMANDS*****


//*****PROCESS COMMANDS*****

    void
    comm_proc_relative_displacement_in_position(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {
        if (args.size() != TX_SIZE::RELATIVE_DISPLACEMENT) {
            throw std::runtime_error(
                    "comm_proc_relative_displacement_in_position() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error(
                    "comm_proc_relative_displacement_in_position() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x44}); // [addr, 0x44]

        if (args[0] != 0x00 && args[0] != 0x01) {
            throw std::runtime_error(
                    "comm_proc_relative_displacement_in_position() invalid " + std::to_string((uint16_t) args[0]));
        }
        set_data(dst, args); //[args]
    }

    void
    comm_proc_absolute_displacement_in_position(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {
        if (args.size() != TX_SIZE::ABSOLUTE_DISPLACEMENT) {
            throw std::runtime_error(
                    "comm_proc_absolute_displacement_in_position() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error(
                    "comm_proc_absolute_displacement_in_position() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x45}); // [addr, 0x45]

        if (args[0] != 0x00 && args[0] != 0x01) {
            throw std::runtime_error(
                    "comm_proc_relative_displacement_in_position() invalid " + std::to_string((uint16_t) args[0]));
        }
        set_data(dst, args); //[args]
    }

    void comm_proc_start_stop_velocity(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {
        if (args.size() != TX_SIZE::START_STOP_VELOCITY) {
            throw std::runtime_error(
                    "comm_proc_start_stop_velocity() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_proc_start_stop_velocity() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x46}); // [addr, 0x46]

        set_data(dst, args); // [args]
    }

//*****PROCESS COMMANDS*****

    void comm_all_stop(modbus_msg_t &dst, uint8_t /*addr*/, const std::vector<uint8_t> &args) {

        if (!args.empty()) {
            throw std::runtime_error(
                    "comm_all_stop() invalid args size " + std::to_string(args.size()));
        }

        set_header(dst, std::vector<uint8_t>{0x00, 0x68}); // [addr, 0x68]
    }

    void comm_debug(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args) {
        if (args.size() != TX_SIZE::DEBUG) {
            throw std::runtime_error("comm_debug() invalid args size " + std::to_string(args.size()));
        }

        if (addr < 0x01 || addr > 0xF7) {
            throw std::runtime_error("comm_debug() invalid " + std::to_string((uint16_t) addr));
        }
        set_header(dst, std::vector<uint8_t>{addr, 0x69}); // [addr, 0x69]

        if (args[0] > 0x03) {
            throw std::runtime_error("comm_debug() invalid " + std::to_string((uint16_t) args[0]));
        }

        set_data(dst, args); // [args]
    }
}
