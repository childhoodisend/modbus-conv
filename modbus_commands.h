#ifndef MODBUS_SAMPLE_MODBUS_COMMANDS_H
#define MODBUS_SAMPLE_MODBUS_COMMANDS_H

#include "modbus_msg.h"

#include <vector>

namespace modbus_conv {

//-----SET COMMANDS-----
    void comm_set_address(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_set_baud(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_set_conf_motor(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_set_conf_el(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_set_limit(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_set_speed_linear_move(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_set_brake_condition(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_req_null_point(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_req_settings_requlator(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);
//*****SET COMMANDS*****


//*****REQUEST COMMANDS*****
    void comm_req_calibration_data(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_req_firmware_and_configuration(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_req_diagnostic(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_req_condition_sensor(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_req_get_pos(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);
//*****REQUEST COMMANDS*****


//*****PROCESS COMMANDS*****
    void comm_proc_relative_displacement_in_position(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_proc_absolute_displacement_in_position(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_proc_start_stop_velocity(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);
//*****PROCESS COMMANDS*****

    void comm_all_stop(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);

    void comm_debug(modbus_msg_t &dst, uint8_t addr, const std::vector<uint8_t> &args);
}

#endif //MODBUS_SAMPLE_MODBUS_COMMANDS_H
