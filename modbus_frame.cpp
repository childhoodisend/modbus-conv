#include "modbus_frame.h"
#include "modbus_msg.h"
#include "modbus_commands.h"
#include "globals.h"

#include <algorithm>
#include <stdexcept>

namespace modbus_conv {

    void set_frame(modbus_msg_t &dst, const std::vector<uint8_t> &src, bool crc_required) {
        if (src.empty()) {
            throw std::runtime_error("set_frame() exc : scr is empty!");
        }

        dst.header.clear();
        dst.data.clear();
        dst.crc.clear();

        size_t n_bytes = src.size();

        // TODO check n_bytes
        if (crc_required) {
            if (n_bytes > 4) {
                dst.header.reserve(2);
                dst.data.reserve(n_bytes - 4);
                dst.crc.reserve(2);

                for (size_t i = 0; i < 2; ++i) {
                    dst.header.push_back(src[i]);
                }
                for (size_t i = 2; i < n_bytes - 2; ++i) {
                    dst.data.push_back(src[i]);
                }
                for (size_t i = n_bytes - 2; i < n_bytes; ++i) {
                    dst.crc.push_back(src[i]);
                }
            } else {
                throw std::runtime_error("set_frame() exc : crc is required, but n_bytes <= 4");
            }
        } else {
            if (n_bytes > 2) {
                dst.header.reserve(2);
                dst.data.reserve(n_bytes - 2);

                for (size_t i = 0; i < 2; ++i) {
                    dst.header.push_back(src[i]);
                }
                for (size_t i = 2; i < n_bytes; ++i) {
                    dst.data.push_back(src[i]);
                }
            } else {
                throw std::runtime_error("set_frame() exc : crc is not required, but n_bites <= 2");
            }
        }
    }

    void set_frame(modbus_msg_t &dst, uint8_t addr, uint8_t code, const std::vector<uint8_t> &args) {

        dst.header.clear();
        dst.data.clear();
        dst.crc.clear();

        if (code == stop_all) {
            comm_all_stop(dst, addr, args);
            return;
        }

        if (std::find(set_comm.begin(), set_comm.end(), code) != set_comm.end()) {
            switch (code) {
                case 0x0A: {
                    if (args[0] == 0x00 && args[1] == 0x01) {
                        comm_set_address(dst, addr, args);
                        return;
                    }
                    if (args[0] == 0xFF && args[1] == 0x00) {
                        comm_set_baud(dst, addr, args);
                        return;
                    }
                    throw std::runtime_error("invalid 0x0A args");
                }
                case 0x64: {
                    comm_set_conf_motor(dst, addr, args);
                    return;
                }
                case 0x65: {
                    comm_set_conf_el(dst, addr, args);
                    return;
                }
                case 0x66: {
                    comm_set_limit(dst, addr, args);
                    return;
                }
                case 0x67: {
                    comm_set_speed_linear_move(dst, addr, args);
                    return;
                }
                case 0x47: {
                    comm_set_brake_condition(dst, addr, args);
                    return;
                }
                default: {
                    throw std::runtime_error("unknown set_comm " + std::to_string((uint16_t) code));
                }
            }
        } else if (std::find(req_comm.begin(), req_comm.end(), code) != req_comm.end()) {
            switch (code) {
                case 0x41: {
                    comm_req_calibration_data(dst, addr, args);
                    return;
                }
                case 0x42: {
                    comm_req_firmware_and_configuration(dst, addr, args);
                    return;
                }
                case 0x43: {
                    comm_req_diagnostic(dst, addr, args);
                    return;
                }
                case 0x48: {
                    comm_req_condition_sensor(dst, addr, args);
                    return;
                }
                case 0x6A: {
                    comm_req_get_pos(dst, addr, args);
                    return;
                }
                case 0x6B: {
                    comm_req_null_point(dst, addr, args);
                    break;
                }
                case 0x6C: {
                    comm_req_settings_requlator(dst, addr, args);
                    break;
                }
                default: {
                    throw std::runtime_error("set_frame : unknown req_comm " + std::to_string((uint16_t) code));
                }
            }
        } else if (std::find(proc_comm.begin(), proc_comm.end(), code) != proc_comm.end()) {
            switch (code) {
                case 0x44: {
                    comm_proc_relative_displacement_in_position(dst, addr, args);
                    return;
                }
                case 0x45: {
                    comm_proc_absolute_displacement_in_position(dst, addr, args);
                    return;
                }
                case 0x46: {
                    comm_proc_start_stop_velocity(dst, addr, args);
                    return;
                }
                case 0x69: {
                    comm_debug(dst, addr, args);
                    return;
                }
                default: {
                    throw std::runtime_error("set_frame : unknown proc_comm " + std::to_string((uint16_t) code));
                }
            }
        } else {
            throw std::runtime_error("set_frame : unknown command " + std::to_string((uint16_t) code));
        }
    }
}
