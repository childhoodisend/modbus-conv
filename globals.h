#ifndef CLIENT_SERVER_GLOBALS_H
#define CLIENT_SERVER_GLOBALS_H

#include <vector>

static const std::vector<uint8_t> codes = {0x0A, 0x64, 0x65, 0x66, 0x67, 0x47, // set
                                           0x41, 0x42, 0x43, 0x48, 0x6A, 0x6B, 0x6C, // request
                                           0x44, 0x45, 0x46, 0x68, 0x69}; // process


static const std::vector<uint8_t> set_comm{0x0A, 0x64, 0x65, 0x66, 0x67, 0x47};
static const std::vector<uint8_t> req_comm{0x41, 0x42, 0x43, 0x48, 0x6A, 0x6B, 0x6C};
static const std::vector<uint8_t> proc_comm{0x44, 0x45, 0x46, 0x69};
static const uint8_t stop_all = 0x68;

namespace RX_SIZE
{
    enum SIZE {
        SLAVE_ADDR_BAUD_RATE = 3,
        CONF_MOTOR = 11,
        CONF_EL = 25,
        LIMIT_DATA = 21,
        RELATIVE_DISPLACEMENT = 5,
        ABSOLUTE_DISPLACEMENT = 5,
        START_STOP_VELOCITY = 5,
        BRAKE_CONDITION = 1,
        CALIBRATION_DATA = 38,
        CALIBRATION = 5, 
        FIRMWARE_AND_CONFIGURATION = 62,
        DIAGNOSTIC = 3,
        SENSOR_READING = 46,
        SPEED_LINEAR_MOVE = 4,
        DEBUG = 1,
        POSITION_REQUEST = 7,
        NULL_POINT = 5,
        NULL_POINT_STATUS = 6,
        REGULATOR_SETTINGS = 13,        
    };
}

namespace TX_SIZE {
    enum SIZE {
        SLAVE_ADDR_BAUD_RATE = 3,
        CONF_MOTOR = 11,
        CONF_EL = 25,
        LIMIT_DATA = 21,
        RELATIVE_DISPLACEMENT = 5,
        ABSOLUTE_DISPLACEMENT = 5,
        START_STOP_VELOCITY = 5,
        BRAKE_CONDITION = 1,
        CALIBRATION = 5, 
        SPEED_LINEAR_MOVE = 4,
        DEBUG = 1,
        NULL_POINT = 5,
        REGULATOR_SETTINGS = 13,
        FIRMWARE_AND_CONFIGURATION = 62,
    };
}


#endif //CLIENT_SERVER_GLOBALS_H
